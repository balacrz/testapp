angular.module('app', ['ngResource', 'ngRoute', 'mainCtrl']);


angular.module('app').config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
  	$locationProvider.html5Mode(true);
    $routeProvider
		.when('/', {
	        templateUrl: '/partials/main',
	        controller: 'mainCtrl'
		})
		.when('/test', {
	        templateUrl: '/partials/test',
	        controller: 'mainCtrl'
		})
  }]
);