var mongoose = require('mongoose');  
var blobSchema = new mongoose.Schema({  
  globalId: String,
  provider: String,
  serviceId: String,
  name: String,
  duration: String,
  cost: String,
  iconType: String,
  latitude: String,
  longitude: String,
  active: Boolean,
  createdBy: String,
  createdById: String,
});
mongoose.model('Service', blobSchema);