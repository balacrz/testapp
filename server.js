var express = require('express');

var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var app = express();

var config = require('./server/config/config')[env];
require('./server/config/express')(app, config);

app.get('/partials/:name', function(req, res){
	res.render('partials/' + req.params.name);
});

app.get('*', function(req, res){
	res.render('index');
})

app.listen(config.port);

console.log('Node is running on ' + config.port + '...');